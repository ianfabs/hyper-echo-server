extern crate hyper;
extern crate futures;

use hyper::{Body, Request, Response, Server};
use hyper::{Method, StatusCode};
use hyper::rt::Future;
use hyper::service::service_fn;
use hyper::Chunk;
use futures::future;
use futures::Stream;

static TEXT: &str = "Hello, World!";

type BoxFut = Box<Future<Item=Response<Body>, Error=hyper::Error> + Send>;

fn echo(req: Request<Body>) -> BoxFut {
    let mut response = Response::new(Body::empty());

    match (req.method(), req.uri().path()) {
        (&Method::GET, "/") => {
            *response.body_mut() = Body::from("Try posting data to /echo");
        },
        (&Method::POST, "/echo") => {
            *response.body_mut() = req.into_body();
        },
        (&Method::POST, "/echo/caps") => {
            let mapping = req
                .into_body()
                .map(|chunk| {
                        chunk.iter()
                            .map(|byte| byte.to_ascii_uppercase())
                            .collect::<Vec<u8>>()
                });
            // Use Body::wrap_stream to convert it to a body
            *response.body_mut() = Body::wrap_stream(mapping); 
        },
        (&Method::POST, "/echo/rev") => {
            let reversed = req
                .into_body()
                .concat2()
                .map(move |chunk| {
                    let body = chunk.iter()
                        .rev()
                        .cloned()
                        .collect::<Vec<u8>>();
                    *response.body_mut() = Body::from(body);
                    response
                });
            return Box::new(reversed);
        },
        (&Method::POST, "/json") => {
        },
        _ => {
            *response.status_mut() = StatusCode::NOT_FOUND;
        },
    };

    Box::new(future::ok(response))
}

fn main(){
    let addr = ([127, 0, 0, 1], 3001).into();

    let server = Server::bind(&addr)
        .serve(|| service_fn(echo))
        .map_err(|e| eprintln!("Server error: {}", e));

    hyper::rt::run(server);
}
